# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  email      :string
#  phone      :string
#  age        :integer
#  gender     :string
#  password   :string
#  image      :string
#  address    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
    attr_accessor :password_confirmation
    validates :email, :phone, uniqueness: true
    validates :password, presence: true, confirmation: true
end
