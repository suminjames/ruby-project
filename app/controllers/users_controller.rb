class UsersController < ApplicationController
  def index
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.create user_params
    if @user.save
      redirect_to users_path
    else
      render :new
    end
  end

  def update
  end

  def destroy
  end

  def show
  end

  private def user_params
    params.require(:user).permit(:name, :email, :age, :gender, :password, :password_confirmation, :phone)
  end
end
